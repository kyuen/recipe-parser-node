import * as request from 'supertest';
import { app } from '../../../src/app';
import { Recipe } from '../../../src/repositories/recipeRepository';

const fakeRecipe: Recipe = {
	categories: [],
	cuisine: "",
	directions: [],
	equipment: [],
	imageUrl: "",
	ingredients: [],
	name: "",
	originalUrl: "http://google.com",
	yield: ""
};
describe('recipe router', () => {
	describe('POST /recipe', () => {
		it('responds with json for the recipe that was added', () => {
			return request(app)
				.post('/recipe')
				.send({ url: 'http://google.com' })
				.expect(200)
				.expect(fakeRecipe);
		});
	});
	describe('GET /recipe', () => {
		it('responds with array of all of the recipes', () => {
			return request(app)
				.get('/recipe')
				.expect(200)
				.expect("all recipes");
		});
	});
});
