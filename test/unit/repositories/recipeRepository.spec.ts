import { expect } from 'chai';
import * as proxyquire from 'proxyquire';
import { Parser } from '../../../src/parsers/parser';
import { RecipeRepository } from '../../../src/repositories/recipeRepository';

let responseHtml: string;
let schemaParserMock: Parser;
const recipeRepo: RecipeRepository = new (proxyquire.noCallThru().load('../../../src/repositories/recipeRepository', {
	'request-promise-native': {
		get: (url: string) => Promise.resolve(responseHtml || url)
	}
})).RecipeRepository();

function formatResponseHtml(itemprop: string, itemValue: string, itemtype: string = "http://schema.org/Recipe") {
	responseHtml = `<html itemtype="${itemtype}">
						<span itemprop='${itemprop}'>${itemValue}</span>
					</html>`;
}

describe('recipeController', () => {
	before(() => {
		// before
	});

	beforeEach(() => {
		// expectedName = "Some awesome recipe";
		// expectedYield = "32 servings";
		// expectedImageUrl = "https://some.image.url";
		// expectedIngredients = ["3 cups flour", "10 oz. butter"];
	});

	describe('when adding recipes', () => {
		it('makes a request to a given a url', async () => {
			const recipeUrl = "https://some.test.url";
			// responseHtml = `<html>
			// 					<ol itemtype='http://schema.org/SomethingElse'>
			// 						<li itemprop='name'>should ignore this name</li>
			// 					</ol>
			// 					<span itemprop='name'>${expectedName}</span>
			// 					<span itemprop='recipeYield'>${expectedYield}</span>
			// 					<span itemprop='image'>${expectedImageUrl}</span>
			// 					<span itemprop='ingredients'>${expectedIngredients[0]}</span>
			// 					<span itemprop='recipeIngredient'>${expectedIngredients[1]}</span>
			// 				</html>`;
			const recipe = await recipeRepo.addRecipe(recipeUrl);
			expect(recipe).to.have.all
				.keys('name', 'ingredients', 'directions', 'equipment',
					'yield', 'categories', 'cuisine', 'originalUrl', 'imageUrl');
			expect(recipe.originalUrl).to.equal(recipeUrl);
		});
		it('passes the raw recipe to the schema parser if it uses http://schema.org/Recipe', async () => {
			const recipeUrl = "https://another.test.url";
			// formatResponseHtml
			const recipe = await recipeRepo.addRecipe(recipeUrl);
		});
		it('passes the raw recipe to the text block parser if the recipe is given in plain text', async () => { });
		it('passes the raw recipe to the recipe json validator if given json', async () => { });
	});
	describe('when getting recipes', () => { });
	describe('when modifying recipes', () => { });
	describe('when deleting recipes', () => { });
});
