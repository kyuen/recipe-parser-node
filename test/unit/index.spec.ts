import { expect, use } from 'chai';
import * as proxyquire from 'proxyquire';
import { stub } from 'sinon';
import * as sinonChai from 'sinon-chai';
use(sinonChai);

const listenStub = stub();
let mocks: any;
describe('index', () => {
	beforeEach(() => {
		mocks = {
			app: { app: { set: stub() } },
			http: { createServer: stub().returns({ listen: listenStub }) }
		};

	});
	describe('when creating server', () => {
		it('uses PORT environment variable', () => {
			process.env.PORT = '4000';
			proxyquire.noCallThru().load('../../src/index', {
				'./app': mocks.app,
				'http': mocks.http
			});
			expect(listenStub).to.have.been.calledWith('4000');
			expect(mocks.app.app.set).to.have.been.calledWith('port', '4000');
			delete process.env.PORT;
		});
		it('defaults to port 5000 if the PORT environment variable is not set', () => {
			proxyquire.noCallThru().load('../../src/index', {
				'./app': mocks.app,
				'http': mocks.http
			});
			expect(listenStub).to.have.been.calledWith(5000);
			expect(mocks.app.app.set).to.have.been.calledWith('port', 5000);
		});
	});
});
