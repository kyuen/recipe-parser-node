import { expect } from "chai";
import { SchemaParser } from "../../../src/parsers/schemaParser";

describe("schema.org recipe parser", () => {
	describe("when parsing a recipe", () => {
		const expectedName = "Some Recipe";
		const expectedYield = "32 servings";
		const expectedImageUrl = "http://some.image.url";
		const expectedIngredients = ["2 tbsp fish sauce", "3 cups water"];
		const expectedCuisine = "Hawaiian";
		const expectedEquipment = ["mixer", "spoon"];
		const expectedDirections = ["Put everything in a bowl.", "Mix it all together."];
		it("uses itemprop attributes to retrieve the different parts", () => {
			const html = `<html itemtype='http://schema.org/Recipe'>
								<span itemprop='name'>${expectedName}</span>
								<span itemprop='recipeYield'>${expectedYield}</span>
								<img src='${expectedImageUrl}' itemprop='image' />
								<span itemprop='ingredients'>${expectedIngredients[0]}</span>
								<span itemprop='recipeIngredient'>${expectedIngredients[1]}</span>
								<span itemprop='recipeCuisine'>${expectedCuisine}</span>
								<span itemprop='tool'>${expectedEquipment[0]}</span>
								<span itemprop='tool'>${expectedEquipment[1]}</span>
								<span itemprop='recipeInstructions'>${expectedDirections[0]}</span>
								<span itemprop='recipeInstructions'>${expectedDirections[1]}</span>
							</html>`;
			const parser = new SchemaParser();
			const recipe = parser.parse(html);
			expect(recipe.name).to.equal(expectedName);
			expect(recipe.yield).to.equal(expectedYield);
			expect(recipe.imageUrl).to.equal(expectedImageUrl);
			expect(recipe.ingredients).to.deep.include({ amount: "2", name: "fish sauce", unit: "tablespoon" });
			expect(recipe.ingredients).to.deep.include({ amount: "3", name: "water", unit: "cup" });
			expect(recipe.equipment).to.include.members(expectedEquipment);
			expect(recipe.directions).to.include.members(expectedDirections);
		});
		it("ignores properties descended from other schemas", () => {
			const html = `<html itemtype='http://schema.org/Recipe'>
								<ol itemtype='http://schema.org/SomethingElse'>
									<li itemprop='name'>should ignore this name</li>
								</ol>
								<span itemprop='name'>${expectedName}</span>
							</html>`;
			const parser = new SchemaParser();
			const recipe = parser.parse(html);
			expect(recipe.name).to.not.equal("should ignore this name");
		});
		it("uses the content attribute for recipeCategory meta tags and the text content for other elements", () => {
			const html = `<html itemtype='http://schema.org/Recipe'>
								<meta itemprop='recipeCategory' content='Appetizer' />
								<span itemprop='recipeCategory'>Entree</span>
							</html>`;
			const parser = new SchemaParser();
			const recipe = parser.parse(html);
			expect(recipe.categories).to.include("Appetizer");
			expect(recipe.categories).to.include("Entree");
		});
		it("it removes extra html before storing the recipe", () => {
			const html = `<html itemtype='http://schema.org/Recipe'>
								<span itemprop='name'><span>Name without extra html</span></span>
							</html>`;
			const parser = new SchemaParser();
			const recipe = parser.parse(html);
			expect(recipe.name).to.equal("Name without extra html");
		});
	});
});
