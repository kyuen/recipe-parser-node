import { json, urlencoded } from 'body-parser';
import * as express from 'express';
import * as logger from 'morgan';
import { RecipeRouter } from './routes/recipeRouter';

class App {
	public express: express.Application;

	constructor() {
		this.express = express();
		this.middleware();
		this.routes();
	}

	private middleware(): void {
		this.express.use(logger('dev'));
		this.express.use(json());
		this.express.use(urlencoded({ extended: false }));
	}

	private routes(): void {
		this.express.use('/recipe', (req, res, next) => {
			const router = new RecipeRouter();
			router.router(req, res, next);
		});
	}
}
const app = new App().express;
export { app };
