import { load } from "cheerio";
import * as ingredientParser from "recipe-ingredient-parser";
import { Ingredient, Recipe } from "../repositories/recipeRepository";
import { Parser } from "./parser";

export class SchemaParser implements Parser {
	private $!: CheerioStatic;
	public parse(html: string): Recipe {
		this.$ = load(html);
		const fakeRecipe: Recipe = {
			categories: this.getCategories(),
			cuisine: this.getElementFromRecipeSchema("recipeCuisine").text(),
			directions: this.getDirections(),
			equipment: this.getEquipment(),
			imageUrl: this.getElementFromRecipeSchema("image").attr("src"),
			ingredients: this.getIngredients(),
			name: this.getElementFromRecipeSchema("name").text(),
			yield: this.getElementFromRecipeSchema("recipeYield").text()
		};
		return fakeRecipe;
	}

	private getElementFromRecipeSchema(itemprop: string) {
		return this.$(`[itemprop='${itemprop}']`).filter((index, element) => {
			return (
				this.$(element)
					.closest("[itemtype]")
					.attr("itemtype") === "http://schema.org/Recipe"
			);
		});
	}

	private getEquipment() {
		const equipment = new Array<string>();
		this.getElementFromRecipeSchema("tool").each((index, element) => {
			equipment.push(this.$(element).text());
		});
		return equipment;
	}

	private getDirections() {
		const directions = new Array<string>();
		this.getElementFromRecipeSchema("recipeInstructions").each((index, element) => {
			directions.push(this.$(element).text());
		});
		return directions;
	}

	private getIngredients() {
		const ingredients = new Array<Ingredient>();
		// Schema.org definition for recipes now uses recipeIngredient instead of ingredients
		this.getElementFromRecipeSchema("ingredients")
			.add(this.getElementFromRecipeSchema("recipeIngredient"))
			.each((index, element) => {
				const parsed = ingredientParser.parse(this.$(element).text());
				const ingredient: Ingredient = {
					amount: parsed.quantity,
					name: parsed.ingredient,
					unit: parsed.unit
				};
				ingredients.push(ingredient);
			});
		return ingredients;
	}

	private getCategories() {
		const categories = new Array<string>();
		this.getElementFromRecipeSchema("recipeCategory").each((index, element) => {
			categories.push(
				element.tagName === "meta"
					? this.$(element).attr("content")
					: this.$(element).text()
			);
		});
		return categories;
	}
}
