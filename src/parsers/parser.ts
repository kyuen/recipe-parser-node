import { Recipe } from "../repositories/recipeRepository";

export interface Parser {
	parse(html: string): Recipe;
}
