import { Request, Response, Router } from 'express';
import { RecipeRepository } from '../repositories/recipeRepository';

export class RecipeRouter {
	public router: Router;
	public recipeRepository: RecipeRepository;

	constructor() {
		this.router = Router();
		this.initRoutes();
		this.recipeRepository = new RecipeRepository();
		console.log('testing');
	}

	public initRoutes(): void {
		this.router.get('/', (req, res) => this.getAll(req, res));
		this.router.post('/', (req, res) => this.add(req, res));
	}

	public getAll(_req: Request, res: Response): void {
		res.send("all recipes");
	}

	public async add(req: Request, res: Response): Promise<void> {
		const url = req.body.url;
		const recipe = await this.recipeRepository.addRecipe(url);
		res.send(recipe);
	}
}
