import * as testing from 'request-promise-native';

export class RecipeRepository {

	public addRecipe(url: string): Promise<Recipe> {
		return testing.get(url).then((html: string) => {
			const recipe: Recipe = {
				categories: [],
				cuisine: '',
				directions: [],
				equipment: [],
				imageUrl: '',
				ingredients: [],
				name: '',
				originalUrl: url,
				yield: '',
			};
			return recipe;
		});
	}
}

export interface Recipe {
	name: string;
	ingredients: Ingredient[];
	directions: string[];
	equipment: string[];
	yield: string;
	categories: string[];
	cuisine: string;
	originalUrl?: string;
	imageUrl: string;
}

export interface Ingredient {
	name: string;
	amount: string;
	unit: string | null;
	recipe?: Recipe;
}
